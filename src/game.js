import { Application, Text, TextStyle, utils } from "pixi.js";
import Explorer from "./explorer.js";
import HealthBar from "./health_bar.js";
import Keyboard from "./keyboard.js";
import Scene from "./scene.js";
import SpriteObject from "./sprite_object.js";


import { contain, hitTestRectangle, randomInt } from "./utils.js";

const TextureCache = utils.TextureCache;

export class Game extends Application {
    constructor() {
        super({
            width: 512,
            height: 512,
        });
        this.renderer.view.style.position = "absolute";
        this.renderer.view.style.top = "50%";
        this.renderer.view.style.left = "50%";
        this.renderer.view.style.transform = "translate(-50%,-50%)";
        document.body.appendChild(this.view);
    }

    load() {
        this.loader.add("./images/treasureHunter.json").load( () => this.setup());
    }

    setup() {
        this.gameScene = new Scene();
        this.stage.addChild(this.gameScene);

        this.gameOverScene = new Scene();
        this.gameOverScene.setVisible(false);
        this.stage.addChild(this.gameOverScene);

        this.dungeon = new SpriteObject(
            TextureCache["dungeon.png"]
        );
        this.gameScene.addChild(this.dungeon);

        this.explorer = new Explorer(
            TextureCache["explorer.png"]
        );
        this.explorer.setPosition(
            68,
            this.stage.height / 2 - this.explorer.height / 2
        );
        this.gameScene.addChild(this.explorer);

        this.treasure = new SpriteObject(
            TextureCache["treasure.png"]
        );
        this.treasure.setPosition(
            this.stage.width - this.treasure.width - 48,
            this.stage.height / 2 - this.treasure.height / 2
        );
        this.gameScene.addChild(this.treasure);

        this.door = new SpriteObject(TextureCache["door.png"]);
        this.door.setPosition(this.door.width, 0);
        this.gameScene.addChild(this.door);

        this.setupBlobs();

        this.healthBar = new HealthBar(this.stage.width - 170, 4);
        this.gameScene.addChild(this.healthBar);

        let style = new TextStyle({
            fontFamily: "Futura",
            fontSize: 64,
            fill: "white",
        });
        this.message = new Text("The End!", style);
        this.message.x = 120;
        this.message.y = this.stage.height / 2 - 32;
        this.gameOverScene.addChild(this.message);

        this.setupController();
        this.ticker.add((delta) => this.loop(delta));
    }

    setupBlobs(numberOfBlobs = 7) {
        this.blobs = [];
        let spacing = 48,
            xOffset = 100,
            speed = 2,
            direction = 1;

        for (let i = 0; i < numberOfBlobs; i++) {
            let blob = new SpriteObject(
                TextureCache["blob.png"]
            );
            let x = spacing * i + xOffset;
            let y = randomInt(
                this.door.height,
                this.stage.height - this.door.height - blob.height
            );
            this.gameScene.addChild(blob);

            blob.setPosition(x, y);
            blob.setVelocity(0, speed * direction);
            direction *= -1;
            this.blobs.push(blob);
        }
        
    }

    loop(delta) {
        let hitBlob = false;
        this.blobs.forEach((blob) => {
            blob.update(delta);

            let blobHitsWall = contain(blob);
            if (blobHitsWall === "top" || blobHitsWall === "bottom") {
                blob.vy *= -1;
            }

            if (hitTestRectangle(this.explorer, blob)) {
                hitBlob = true;
            }
        });

        if (hitBlob && !this.explorerHit) {
            this.explorerHit = true;
            this.healthBar.updateHealth(-30);

            let interval = setInterval(() => {
                if (this.explorer.alpha == 1) this.explorer.alpha = 0.5;
                else this.explorer.alpha = 1;
            }, 200);

            setTimeout(() => {
                clearInterval(interval);
                this.explorer.alpha = 1;
                this.explorerHit = false;
            }, 1200);
        }

        if (hitTestRectangle(this.explorer, this.treasure)) {
            this.treasure.setPosition(this.explorer.x + 8, this.explorer.y + 8);
        }

        if (hitTestRectangle(this.treasure, this.door)) {
            this.end();
            this.message.text = "You won!";
            this.ticker.stop();
        }

        if (this.healthBar.health < 0) {
            this.end();
            this.message.text = "You lost!";
            this.ticker.stop();
        }
        this.explorer.update(delta);
    }

    end() {
        this.gameScene.setVisible(false);
        this.gameOverScene.setVisible(true);
    }

    setupController() {
        let left = new Keyboard("ArrowLeft"),
            up = new Keyboard("ArrowUp"),
            right = new Keyboard("ArrowRight"),
            down = new Keyboard("ArrowDown");
        
        let verticalMoveRelease = () =>{
            this.explorer.move("stop_vertical");
        };

        let horizontalMoveRelease = () => {
            this.explorer.move('stop_horizontal');
        }

        left.setRelease(horizontalMoveRelease);
        right.setRelease(horizontalMoveRelease);

        up.setRelease(verticalMoveRelease);
        down.setRelease(verticalMoveRelease);

        left.setPress(() => {
            this.explorer.move('left')
        });

        up.setPress(() => {
                this.explorer.move('up');
        });

        right.setPress(() => {
            this.explorer.move('right');
        });

        down.setPress(() => {
            this.explorer.move('down');
        });
    }
}
