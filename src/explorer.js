import SpriteObject from "./sprite_object";

export default class Explorer extends SpriteObject {
    constructor(textureCache, speed = 4) {
        super(textureCache);
        this.speed = speed;
        this.verticalDirection = 0;
        this.horizontalDirection = 0;
        this.setVelocity(0, 0)
    }

    update(delta) {
        let addX =  this.vx * delta;
        let addY = this.vy * delta;

        if(this.x + addX >= 28 && this.x + addX <= 512 - 50)
            this.x += addX;
        if(this.y + addY >= 20 && this.y + addY <= 512 - 65)
            this.y += addY;
    }

    move(direction) {
        switch (direction) {
            case "left":
                this.horizontalDirection = -1;
                break;
            case "right":
                this.horizontalDirection = 1;
                break;
            case "up":
                this.verticalDirection = -1;
                break;
            case "down":
                this.verticalDirection = 1;
                break;
            case "stop_vertical":
                this.verticalDirection = 0;
                break;
            case "stop_horizontal":
                this.horizontalDirection = 0;
        }

        let sp = 0;
        if (this.horizontalDirection == 0 || this.verticalDirection == 0) {
            sp = this.speed;
        } else {
            sp = this.speed * Math.sin(Math.PI / 4);
        }
        this.setVelocity(
            this.horizontalDirection * sp,
            this.verticalDirection * sp
        );
    }
}
